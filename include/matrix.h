#ifndef MATRIX_H
#define MATRIX_H

#include <stdint.h>

#define MATRIX_BORDER "|"

/*! \struct Matrix
    \brief A struct defining two dimensional array that represent matrix.
*/
typedef struct {
	uint32_t rows;
	uint32_t columns;
	double* content;
} Matrix;

/*! \fn matrix_init
	\brief initialize a new matrix object.
	\param matrix an object of one dimensional array that represent a matrix.
	\param rows the number of rows in the \a matrix object.
	\param columns the number of columns in the \a matrix object.
*/
void matrix_init(Matrix* matrix, uint32_t rows, uint32_t columns);


/*! \fn matrix_create
	\brief create and return a new matrix object.
	\param rows the number of rows in the matrix object.
	\param columns the number of columns in the matrix object.
	\return A new matrix object.
*/
Matrix* matrix_create(uint32_t rows, uint32_t columns);


/*! \fn matrix_get_index
	\brief Get the index inside an array using \a row and \a column.
	\param matrix a matrix object of one dimensional array.
	\param row the row in the \a matrix object.
	\param column the column in the \a matrix object.
	\return The index of the given \a row and \a column.
*/
uint32_t matrix_get_index(const Matrix* matrix, uint32_t row, uint32_t column);


/*! \fn matrix_get_at
	\brief Get the \a matrix value at a certain point.
	\param matrix a matrix object of one dimensional array.
	\param row the row in the \a matrix object.
	\param column the column in the \a matrix object.
	\return The value at the the given \a row and \a column.
*/
double matrix_get_at(const Matrix* matrix, uint32_t row, uint32_t column);


/*! \fn matrix_set_at
	\brief Set the matrix value at a \a row and \a column.
	\param matrix a matrix object of one dimensional array.
	\param row the row in the \a matrix object.
	\param column the column in the \a matrix object.
	\param value the value to set in the \a matrix object at \a row and \a column.
*/
void matrix_set_at(Matrix* matrix, uint32_t row, uint32_t column, double value);


/*! \fn matrix_size
	\brief Calculates and returns the size of the \a matrix.
	\param matrix a matrix object of one dimensional array.
	\return The size of the \a matrix.
*/
uint32_t matrix_size(const Matrix* matrix);


/*! \fn matrix_read
	\brief Read values into a \a matrix object.
	\param matrix a matrix object of one dimensional array.
*/
void matrix_read(Matrix* matrix);


/*! \fn matrix_print
	\brief Print the \a matrix object. 
	\param matrix an object of two dimensional array.
*/
void matrix_print(const Matrix* matrix);


/*! \fn matrix_mult
	\brief Multiply two matrixes together.
	\param matrix a matrix object of one dimensional array.
	\param other another matrix object of one dimensional array.
	\return A new heap allocated matrix of the multiplication result.
*/
Matrix* matrix_mult(const Matrix* mat1, const Matrix* mat2);


/*! \fn matrix_sum
	\brief Sum two matrixes together. the first \a matrix will get the final result.
	\param matrix a matrix object of one dimensional array.
	\param other another matrix object of one dimensional array.
*/
void matrix_sum(Matrix* matrix, const Matrix* other);


/*! \fn matrix_scalar_mult
	\brief Multiply a matrix with scalar number.
	\param matrix a matrix object of one dimensional array.
	\param value the scalar value to multiply the \a matrix with.
*/
void matrix_scalar_mult(Matrix* matrix, double value);


/*! \fn matrix_determinant
	\brief Calculate and return the matrix determinant value.
	\param matrix a matrix object of one dimensional array.
	\return The matrix determinant.
*/
double matrix_determinant(const Matrix* matrix);

#endif /* MATRIX_H */
