CC=gcc
CFLAGS=-g -Wall -Iinclude
OBJS= obj/matrix.o obj/main.o

ifdef OS
	CCMD=del /q bin\* obj\*
else
	CCMD=rm -rf bin/* obj/*
endif

all: matrix

obj/%.o: src/%.c include/%.h | obj
	$(CC) $(CFLAGS) -c "$<" -o "$@"

bin/matrix: ${OBJS} | bin
	$(CC) $(CFLAGS) -lm -o $@ ${OBJS}

bin obj:
	mkdir "$@"

matrix: bin/matrix

clean:
	$(CCMD)
