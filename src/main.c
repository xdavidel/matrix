#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "main.h"

#define MENU_EXIT        '0'
#define MENU_MULT        '1'
#define MENU_DETERMINANT '2'
#define MENU_SUM         '3'
#define MENU_SCALAR      '4'

void mult();
void sum();
void scalar();
void determinant();
void print_menu();

char *choices[] = {
			"Multiply matrix",
			"Calculete matrix determinant",
			"Sum matrix together",
			"Multiply matrix in scalar",
			"Exit",
		  };

uint32_t n_choices = sizeof(choices) / sizeof(char *);


int main() {

    char op = -1;

    // TODO: replace with ncurses
    puts("Welcome to Matrix V5 - by David Delarosa");
	puts("____________________________________________________\n");

	do {
        print_menu();

        scanf("%c", &op);

		switch (op) {
		case MENU_EXIT:
			continue;
		case MENU_MULT:
            mult();
			break;
		case MENU_DETERMINANT:
            determinant();
			break;
		case MENU_SUM:
            sum();
			break;
		case MENU_SCALAR:
            scalar();
			break;
		default:
			break;
		}

	} while (op != '0');

	return 0;
}

void
print_menu() {
    uint32_t i;

	for(i = 0; i < n_choices; i++) {
        printf("%d - %s\n", (i + 1) % n_choices, choices[i]);
	}
}


void
mult() {
	puts("Matrix Multiplication\n\
____________________________________________________\n"
    );

	uint32_t number, i;
	uint32_t rows, cols;
    Matrix *temp, *t;

	printf("Enter the number to multiply: ");
	scanf("%d", &number);

	printf("Enter first matrix size (N x K): ");
	scanf("%d %d", &rows, &cols);

	Matrix* result = matrix_create(rows, cols);
	printf("Enter %d values: ", matrix_size(result));

    matrix_read(result);

	for (i = 1; i < number; i++) {
		printf("Enter matrix #%d columns: ", i + 1);
		scanf("%d", &cols);
        temp = matrix_create(result->columns, cols);
		printf("Enter %d values for matrix #%d: ", matrix_size(temp), i + 1);
		matrix_read(temp);

        t = matrix_mult(result, temp);
        free(result);
        result = t;
        free(temp);
	}

	puts("The result is:");
    matrix_print(result);
    free(result);
}

void
sum() {
	puts("Matrix Summarize\n\
____________________________________________________\n"
    );

	uint32_t number, i;
	uint32_t rows, cols;
    Matrix* temp;

	printf("Enter the number to sum: ");
	scanf("%d", &number);

	printf("Enter first matrix size (N x K): ");
	scanf("%d %d", &rows, &cols);

	Matrix* result = matrix_create(rows, cols);
	printf("Enter %d values: ", matrix_size(result));

    matrix_read(result);

	for (i = 1; i < number; i++) {
        temp = matrix_create(result->columns, cols);
		printf("Enter %d values for matrix #%d: ", matrix_size(temp), i + 1);
		matrix_read(temp);

        matrix_sum(result, temp);
        free(temp);
	}

	puts("The result is:");
    matrix_print(result);
    free(result);
}

void
scalar() {
	puts("Matrix Scalar Multiplication\n\
____________________________________________________\n"
    );

	uint32_t rows, cols;
	double value;

	printf("Enter matrix size (N x K): ");
	scanf("%d %d", &rows, &cols);

	Matrix* result = matrix_create(rows, cols);
	printf("Enter %d values: ", matrix_size(result));

    matrix_read(result);

	printf("Enter a scalar value to multiply with: ");
    scanf("%lf", &value);

    matrix_scalar_mult(result, value);

	puts("The result is:");
    matrix_print(result);

    free(result);
}

void determinant() {
	puts("Matrix Determinant Calculator\n\
____________________________________________________\n"
    );

	uint32_t rows;

	printf("Enter matrix size (N x N): ");
	scanf("%d", &rows);

	Matrix result;
    matrix_init(&result ,rows, rows);
    printf("Enter %d values: ", matrix_size(&result));
    matrix_read(&result);

	puts("The determinant of the matrix:");
    matrix_print(&result);
    printf("is: %lf\n", matrix_determinant(&result));
}
