#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "matrix.h"

void
matrix_init(Matrix* matrix, uint32_t rows, uint32_t columns) {
    matrix->rows = rows;
    matrix->columns = columns;

    matrix->content = (double*)calloc(rows * columns, sizeof(double));
}

Matrix*
matrix_create(uint32_t rows, uint32_t columns) {
    Matrix* matrix = (Matrix*)malloc(sizeof(Matrix));
    matrix->rows = rows;
    matrix->columns = columns;

    matrix->content = (double*)calloc(rows * columns, sizeof(double));

    return matrix;
}

uint32_t
matrix_get_index(const Matrix* matrix,
                              uint32_t row,
                              uint32_t column) {
    return row * matrix->columns + column;
}

double
matrix_get_at(const Matrix* matrix,
                              uint32_t row,
                              uint32_t column) {
    uint32_t index;
    
    index = matrix_get_index(matrix, row, column);

    return matrix->content[index];
}

void 
matrix_set_at(Matrix* matrix,
                   uint32_t row,
                   unsigned column,
                   double value) {
    uint32_t index;
    
    index = matrix_get_index(matrix, row, column);
    matrix->content[index] = value;
}

uint32_t 
matrix_size(const Matrix* matrix) {
    return matrix->rows * matrix-> columns;
}

void 
matrix_read(Matrix* matrix) {
    uint32_t row, col;
    double value;
	for (row = 0; row < matrix->rows; row++) {
		for (col = 0; col < matrix->columns; col++) {
			scanf("%lf", &value);
			matrix_set_at(matrix, row, col, value);
		}
	}
}

void 
matrix_print(const Matrix* matrix) {
    uint32_t row, column, index;
    puts("");
	for (row = 0; row < matrix->rows; row++) {
        printf("%s ", MATRIX_BORDER);
		for (column = 0; column < matrix->columns; column++) {
            index = matrix_get_index(matrix, row, column);
            printf("%-10.3lf ", matrix->content[index]);
		}
        printf(" %s\n", MATRIX_BORDER);
	}
}

Matrix* 
matrix_mult(const Matrix* mat1, const Matrix* mat2) {
    // check for compatability
    if (mat1->columns != mat2->rows) {
        fprintf(stderr,
                "Multiplication of (%dx%d) with (%dx%d) is not possible\n",
                mat1->rows, mat1->columns, mat2->rows, mat2->columns);
        return (Matrix*)mat1;
    }

    uint32_t i, j, x;
    double value;

    Matrix* result = matrix_create(mat1->rows, mat2->columns);

    for (i = 0; i < mat1->rows; i++) {
        for (j = 0; j < mat2->columns; j++) {
            value = 0;
            for (x = 0; x < mat1->columns; x++) {
                value += matrix_get_at(mat1, i, x) *
                         matrix_get_at(mat2, x, j);
            }
            matrix_set_at(result, i, j, value);
        }
    }

    return result;
}

void 
matrix_sum(Matrix* matrix, const Matrix* other) {
    // check for compatability
    if (matrix->rows != other->rows || matrix->columns != other->columns) {
        fprintf(stderr,
                "Sum of (%dx%d) with (%dx%d) is not possible\n",
                matrix->rows, matrix->columns, other->rows, other->columns);
        return;
    }

    uint32_t i;

    for (i = 0; i < matrix_size(matrix); i++) {
        matrix->content[i] += other->content[i];
    }
}

void 
matrix_scalar_mult(Matrix* matrix, double value) {
    uint32_t i;

    for(i = 0; i < matrix_size(matrix); i++) {
        matrix->content[i] *= value;
    }
}

double 
matrix_determinant(const Matrix* matrix) {
	if (matrix->rows != matrix->columns) {
        fprintf(stderr, "Determinant exist only for (N x N) matrix\n");
        return 0;
	}

	if (2 == matrix->rows) {
		// ad - bc
		return (matrix->content[0] * matrix->content[3]) -
            (matrix->content[1] * matrix->content[2]);
	}
	else {
        uint32_t x, subi, subj, i, j;
		double det = 0;
		Matrix submatrix;
        matrix_init(&submatrix, matrix->rows - 1, matrix->columns - 1);
		for (x = 0; x < matrix->rows; x++) {
			subi = 0;
			for (i = 1; i < matrix->rows; i++) {
				subj = 0;
				for (j = 0; j < matrix->rows; j++) {
					if (j != x) {
                        matrix_set_at(&submatrix, subi, subj,
                                matrix_get_at(matrix, i, j));
						subj++;
					}
				}
				subi++;
			}
			det = det + (
                    pow(-1, x) *
                    matrix_get_at(matrix, 0, x) *
                    matrix_determinant(&submatrix));
		}

		return det;
	}
}
